package test;

import static org.junit.jupiter.api.Assertions.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import hotel.credit.CreditCard;
import hotel.credit.CreditCardType;
import hotel.entities.Booking;
import hotel.entities.Guest;
import hotel.entities.Room;
import hotel.entities.RoomType;
import hotel.entities.ServiceType;

class BookingTest {

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	void testAddServiceCharge() {
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
		Date date = new Date();
		try {
			date = format.parse("01-01-2000");
		} catch (ParseException e) {
			
		}
		Guest guest = new Guest("Sam", "Creak", 222);
		Room room = new Room(101, RoomType.SINGLE);
		CreditCard card = new CreditCard(CreditCardType.VISA, 1, 1);
		Booking booking = new Booking(guest, room, date, 2, 1, card);
		booking.addServiceCharge(ServiceType.ROOM_SERVICE, 10);
		assertEquals(10, booking.getCharges().get(0).getCost());
		booking.addServiceCharge(ServiceType.RESTAURANT, 100);
		assertEquals(100, booking.getCharges().get(1).getCost());
	}

}
