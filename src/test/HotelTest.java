package test;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.junit.jupiter.api.Test;

import hotel.credit.CreditCard;
import hotel.credit.CreditCardType;
import hotel.entities.Booking;
import hotel.entities.Guest;
import hotel.entities.Hotel;
import hotel.entities.Room;
import hotel.entities.RoomType;
import hotel.entities.ServiceType;

class HotelTest {

	@Test
	final void testAddServiceCharge() {
		Map<Integer, Booking> activeBookingsByRoomId = mock(Map.class);
		Booking booking = mock(Booking.class);
		when(activeBookingsByRoomId.get(anyInt())).thenReturn(booking);
		Hotel hotel = new Hotel();
		hotel.activeBookingsByRoomId = activeBookingsByRoomId;
		hotel.addServiceCharge(101, ServiceType.ROOM_SERVICE, 50.0);
		verify(booking, times(1)).addServiceCharge(ServiceType.ROOM_SERVICE, 50.0);
	}
	
	@Test
	final void testCheckout() {
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
		Date date = new Date();
		try {
			date = format.parse("01-01-2000");
		} catch (ParseException e) {
			
		}
		Guest guest = new Guest("Sam", "Creak", 222);
		CreditCard card = new CreditCard(CreditCardType.VISA, 1, 1);
		Hotel hotel = new Hotel();
		hotel.addRoom(RoomType.SINGLE, 101);
		Room room = hotel.findAvailableRoom(RoomType.SINGLE, date, 1);
		long confNo = hotel.book(room, guest, date, 1, 2, card);
		Booking booking = hotel.findBookingByConfirmationNumber(confNo);
		hotel.checkin(confNo);
		hotel.checkout(101);
		assertEquals(false, hotel.activeBookingsByRoomId.containsKey(101));
	}

	
}
